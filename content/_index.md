---
title: "Welcome"
---

# Welcome to my archived Wiki
This is a **knowledge base** where [I](https://frederikberg.com) once wrote down stuff I learned about Linux servers, Nextcloud, and other self-hostable services. I started this Wiki to make some of my notes and knowledge available to others in the hope that it will be **useful to someone**.

I have **not updated this Wiki for a long time** (since ~2022), so information may be **outdated** but I am leaving it **here for reference**.

**Content:**
If you want to set up your own **home server** (Raspberry Pi or Linux server), you can start with
- The page about Linux servers [docs/Linux Server]({{< ref "docs/Linux Server" >}})
- Continue with the page about networks and how to make your server accessible [docs/Network]({{< ref "docs/Network" >}})
- And follow up with installing interesting apps via docker, like Pi-Hole, Nextcloud or Bitwarden [docs/Docker and Server Apps]({{< ref "docs/Docker and Server Apps" >}})
- Finally, it is always important to make regular backups [docs/Linux Backup]({{< ref "docs/Linux Backup" >}})

---
All pages
- [Linux General]({{< ref "docs/Linux General" >}})
- [Linux Backup]({{< ref "docs/Linux Backup" >}})
- [Linux Server]({{< ref "docs/Linux Server" >}})
- [Network]({{< ref "docs/Network" >}})
- [Docker and Server Apps]({{< ref "docs/Docker and Server Apps" >}})
- [Other OS]({{< ref "../docs/Other OS" >}})
- [Other]({{< ref "docs/Other" >}})