---
title: "Linux Backup"
---

# Linux Backup

## Clonezilla Backups
You can use clonezilla to make full backups of your complete drive or the Raspberry SD card
https://clonezilla.org/

## Kopia Backups
- To backup all kind of data I use [Kopia](https://kopia.io)
	- It supports incremental backups, which allow to restore to different points in time
- [Install Kopia](https://kopia.io/docs/installation/#linux-installation-using-apt-debian-ubuntu)
- [Guide on how to use Kopia](https://kopia.io/docs/getting-started/)

- There are basically two backup mechanisms: push and pull
	- With push, the client containing the data is pushing the data to a backup server
	- With pull, the backup server is asking a client to get its data to backup it
- Kopia only supports the push mode (so the client side mode)

- On our client, containing the data, we initialize a repository, which will be located on our backup driver or also backup server
- For a backup drive simply create a repo via
	- `kopia repository create filesystem --path /path/to/backup/drive/folder`
- We create and connect to a backup server repository e.g. via ssh (sftp)
	- We already have to have a working ssh connection from our client to our server with ssh keys set up
	- `kopia repository create sftp --path=/home/freddy/Backup/pi4 --host=freddy-desktop.lan --username=freddy --keyfile=/root/.ssh/id_rsa --known-hosts=/root/.ssh/known_hosts`
- That is all, now we are connected via Kopia to this backup repository
	- Kopia will always reconnect to this reposiotory, except if you manually disconnect at some point via `kopia repository disconnect`
	- Then you can reconnect via `kopia repository connect filesystem --path /path/to/backup/drive/folder`
		- or via `kopia repository connect sftp --path=/home/freddy/Backup/pi4 --host=freddy-desktop.lan --username=freddy --keyfile=/root/.ssh/id_rsa --known-hosts=/root/.ssh/known_hosts`

### Snapshot
- Now we can create backups (snapshots) of various data located on our client
- Examples
	- `kopia snapshot create /opt/docker`
	- or if you want to first dump a database from a docker container, to be sure that it is in a valid state when restoring do e.g.:
		- `docker exec postgres-synapse pg_dumpall -U synapse > /databases/docker-synapse-postgres.dump && kopia snapshot create /databases/docker-synapse-postgres.dump && rm /databases/docker-synapse-postgres.dump`
	- `kopia snapshot create /mnt/data/nextcloud`

#### Ignore
- You can also ignore specific directories during the backup process
	- Example: `sudo kopia policy set --add-ignore '*' /home/frederik/Nextcloud /home/frederik/sciebo/`

### Sync Backup to another Backup Repository
- If we want to sync our backup from one backup repository to another this is also possible with kopia
	- [Forum question about this](https://kopia.discourse.group/t/duplicate-repo-to-another-location/1144/3)
- Connect to the source repository if not already connected
	- `kopia repository connect filesystem --path=/home/freddy/Backup/pi4`
- Now you can sync the repo to the target repository (must be empty folder or have the same `kopia.repository` file) and specify it via path
	- `kopia repository sync-to filesystem --path /media/freddy/Backup_Freddy/backup-server`
	- Here you can also use sftp instead of the filesystem again

### Mount / Restore Backup
- You can mount the backup to check the files
- `kopia mount snapshot-id ~/my-mount-location`
- `kopia restore snapshot-id ~/my-mount-location`

### My Server Backups
- On my main server, I have an additional backup drive
- My main server and my other Raspberry Pi both connect to this same backup drive via kopia and do regular backups via cron every 1h
- Since it is risky to only have one backup repository, I also sync the repository to my PC ones per Day via ssh, such that the complete repository is also available at a drive of my PC
- And again I have another external drive, which I sometimes attach to my PC to also sync the repository on my PC to the external drive, to have another 3rd backup location
- The backup scripts are also available in my repos
	- [Server scripts](https://gitlab.com/frederikb96/linux-server)
	- [Scripts on my PC](https://gitlab.com/frederikb96/Linux-Scripts)

### My Home Backups
- Use Kopia
	- `sudo kopia repository create filesystem --path /media/frederik/Backup-i11/backup-i11`
- Frequency
	- `sudo kopia policy set --global --keep-annual 1 --keep-monthly 3`
- Ignore folders
	- `sudo kopia policy set --add-ignore '*' /home/frederik/Nextcloud /home/frederik/sciebo/`
	- `sudo kopia policy set --global --add-ignore '.cache' --add-ignore 'cache'`
- Test it
	- `sudo kopia snapshot estimate /home/frederik`
- Use a launcher to launch it easily or automate the process with a script
- I, personally, have a script run by anacron once a day, which automatically checks the network interface, if that is the home network, it connects to a backup server and triggers a backup of the home directory. Additionally, it backs up a list of all installed programs. A notification is sent to the desktop, if the backup is done or if it fails, with a link to the log file. [See here for the script](https://gitlab.com/frederikb96/Linux-Scripts/-/tree/master/bin)

Backup to smb drive:
- [mount to local filesystem](https://wiki.ubuntu.com/MountWindowsSharesPermanently)
- also add "nofail" if drive not always available
- then do normal kopia backup to filesystem

### Android Backups
We want to back up everything under
- `/sdcard` - normal user data
- `/data/data` - internal private app data, not visible to normal user

#### Backup
Prepare ssh acces on android phone
[Other OS#SSH Server on android]({{< ref "Other OS#ssh-server-on-android" >}})

So we will prepare a script for the backup server and then we will prepare android to start this script

On backup server site:
- Create kopia backup repository
- mount android via sshfs `sshfs root@192.168.0.23:/data/data/ /mnt/android/data-data -p 2222`
	- `sshfs root@192.168.0.23:/sdcard/ /mnt/android/sdcard -p 2222`
- And now you can create snapshots
	- `kopia snapshot create /mnt/android/data-data/`
	- `kopia snapshot create /mnt/android/sdcard/`
- And now umount again
	- `umount /mnt/android/data-data`
	- `umount /mnt/android/sdcard`
- This can be put into a script, which automates this process

On Android site:
- We want to automate the backup, by calling the script on the server side by using the tasker app
- You can use tasker to create a new task
	- first launch app action to start the simplessh app
	- then a run shell action to ssh into the server and start the backup script
		- `/data/data/com.termux/files/usr/bin/ssh root@192.168.0.100 /root/scripts/backup-android.sh`
	- now you can kill the sshd app
	- And send a notification that the backup is done
		- Optionally with the result of the script, which can be saved in a file and also stored in a tasker variable to be displayed via the notification

Example tasker process:
```
Task: Backup-android
    
    A1: Launch App [
         Package/App Name: SimpleSSHD ]
    
    A2: Run Shell [
         Command: {
         /data/data/com.termux/files/usr/bin/ssh root@192.168.0.100 /root/scripts/my-backup-android.sh
         } >  /sdcard/.tmp-backup-android.log 2>&1
         Timeout (Seconds): 0
         Use Root: On
         Use Global Namespace: On
         Continue Task After Error:On ]
    
    A3: Kill App [
         App: SimpleSSHD
         Use Root: On ]
    
    A4: Run Shell [
         Command: /data/data/com.termux/files/usr/bin/tail -n 5 /sdcard/.tmp-backup-android.log
         Timeout (Seconds): 0
         Use Root: On
         Store Output In: %res
         Use Global Namespace: On ]
    
    A5: Notify [
         Title: Backup Done
         Text: %res
         Icon: hd_device_access_storage
         Number: 0
         Priority: 3
         LED Colour: Green
         LED Rate: 0
         Category: Backup ]
```

#### Restore
If restoring make sure, that the correct owners are set for the `/data/data/` folders for each app
- every installed app has its own linux user and this must fit to the restored data folder
	- If reinstalling apps, this user changes
	- So reinstall an app and check the user ID: `ls -la /data/data/com.your-app`
	- And after restoring with kopia, for example via:
		- `sshfs root@192.168.0.23:/data/data/ /mnt/android/data-data -p 2222`
		- `mkdir -p /mnt/some-tmp-location`
		- `kopia snapshot restore /mnt/android/data-data/us.zoom.videomeetings --snapshot-time=latest /mnt/some-tmp-location`
		- `chown -R 10335:10335 /mnt/some-tmp-location`
		- `chown -R 10335:20335 data/data/us.zoom.videomeetings/code_cache/` - necessary since cache has a different group
		- And now restoring
		- `rsync -av ./mnt/some-tmp-location/us.zoom.videomeetings/ /mnt/android/data-data/us.zoom.videomeetings/ --delete`
- Restore other external data from sdcard, the same way
- You can use the script I provided in my scrips repo for automation
- Also create a tasker task analog to the backup task
	- Just specifc a tasker variable in additon, which specifys the app name, that should be restored

## Borg Backups
Borg is similar to Kopia and is being used by the nextcloud-aio container to do automatic backups. Here are some hints on how to restore backups manually if need be. Normally, one would use the built-in restore tool from nextcloud-aio, though.

- Get info of borg repo
	- `sudo borg info /mnt/backup/backup-nextcloud-aio/borg`
	- `sudo borg list /mnt/backup/backup-nextcloud-aio/borg`
- To restore from borg backup files to current directory (remove --dry-run)
`sudo borg extract --list --dry-run /mnt/backup/backup-nextcloud-aio/borg::20230219_030107-nextcloud-aio nextcloud_aio_volumes/nextcloud_aio_nextcloud_data/Freddy/files/some-file`

## Unison Backups
**I recently switched to use Kopia for backups, so I would recommend kopia instead**

Use unison as file sync program [unison](https://www.cis.upenn.edu/~bcpierce/unison/)
[Manual for unison](https://www.cis.upenn.edu/~bcpierce/unison/download/releases/stable/unison-manual.html)

Install it via:
```
sudo apt install unison
```

But same version on host and client needed, so maybe install .deb manually and download somewhere if both Linux versions do not have the same version
https://pkgs.org/download/unison

A unison config for a nextcloud backup:
```
# Unison preferences
label = Nextcloud Backup
root = ssh://root@pi4.lan//mnt/data/nextcloud
force = ssh://root@pi4.lan//mnt/data/nextcloud
root = /home/freddy/Backup/nextcloudbackup/data
batch = true
group = false
owner = false
perms = -1
times = true
links = true
retry = 1
logfile = /home/freddy/Computer/Pi_Backup_Log/nextcloud_unison.log
```

And file for backups to an external drive
```
# Unison preferences
label = External Backup
root = /home/freddy/Backup
force = /home/freddy/Backup
root = /media/freddy/Backup_Freddy/pi_Backups
ignore = Path lost+found
ignore = Path .Trash-1000
auto = true
group = false
owner = false
perms = -1
times = true
links = true
retry = 1
log = false
```

You can execute unison backup manually via:
```
unison my-profile-name
```
